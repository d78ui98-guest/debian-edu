#!/bin/sh

error() {
    print "error: " $@ 1>&2
}

setup_tasksel_overrides() {
    # Modify tasksels desktop test so the normal Debian desktop
    # profiles are not installed. Use dpkg-divert to to switch to our
    # version
    sed -e 's/if desktop_hardware/# Do not install a desktop - added by debian-edu-install\nunmark\n\n&/' \
        < /usr/lib/tasksel/tests/desktop \
        > /usr/lib/tasksel/tests/desktop.edu
    chmod 755 /usr/lib/tasksel/tests/desktop.edu
    dpkg-divert --package debian-edu-install --rename --quiet \
        --add /usr/lib/tasksel/tests/desktop
    ln -sf ./desktop.edu /usr/lib/tasksel/tests/desktop

    # And for the standard system task too
    sed -e 's/^case/# Do not install standard system task - added by debian-edu-install\nexit 3\n\n&/' \
        < /usr/lib/tasksel/tests/new-install \
        > /usr/lib/tasksel/tests/new-install.edu
    chmod 755 /usr/lib/tasksel/tests/new-install.edu
    dpkg-divert --package debian-edu-install --rename --quiet \
        --add /usr/lib/tasksel/tests/new-install
    ln -sf ./new-install.edu /usr/lib/tasksel/tests/new-install
}

remove_tasksel_overrides() {
    for test in desktop new-install ; do
        file=/usr/lib/tasksel/tests/$test
        if [ -x $file.edu ] ; then
            rm $file
            dpkg-divert --package debian-edu-install \
                --rename --quiet --remove $file
            rm $file.edu
        else
            error "Missing divert for $file."
        fi
    done
}

case "$1" in
    setup)
	setup_tasksel_overrides
	;;
    teardown)
	remove_tasksel_overrides
	;;
    tasksel)
	setup_tasksel_overrides
	shift
	tasksel $@
	remove_tasksel_overrides
esac
